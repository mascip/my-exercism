# A simple workflow #

For each exercise:
 - Check out the README and test file
 - Create the code file
 - in another terminal, `$ lein repl`, to start the REPL. From inside the repl do (use 'namespace-name) to be able to use my functions in the REPL
 - and in another terminal `$ lein test-refresh` to see at all times whether the tests pass, and what is the next thing to fix.
