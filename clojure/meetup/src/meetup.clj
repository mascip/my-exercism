(ns meetup
  (:require
    ; [clojure.core :refer :all]
    [clj-time.core :as date]
    [medley.core :refer [find-first]]
    [clojure.string :as s]
  ))

(def daynum->dayname {1 "monday", 2 "tuesday", 3 "wednesday", 4 "thursday" 5 "friday" 6 "saturday" 7 "sunday"})

; (def weekdays (keys dayname->fn-prefix))
(def weekdays (vals daynum->dayname))

(def week-number {"first" 1, "second" 2, "third" 3, "fourth" 4})

(def weektypes ["first" "second" "third" "fourth" "teenth" "last"])

(defn dayname->fn-prefix [weekday]
  "Get rid of the last three letters: d, a, y"
  (-> weekday s/reverse (subs 3) s/reverse))

(defn is-weekday? [a-date weekday]
  "Is it a monday?"
  ; (redl.core/break)
  (= weekday (-> a-date date/day-of-week daynum->dayname)))

(defn week-num-of [a-date]
  "Is it the first monday of the month? The second?"
  (-> a-date date/day dec ; dec, because the day number 7 is still in the first week
      (quot 7) inc)       ; inc, because the first week is week 1
)

(defn is-in-week-num? [a-date week-num]
  "Ex: Is this date in the first week?"
  (= week-num (week-num-of a-date)))

(defn is-in-teenthweek? [a-date]
  (<= 13 (date/day a-date) 19))

(defn is-in-last-week? [a-date]
  (let [year (date/year a-date)
        month (date/month a-date)]
    (> (date/day a-date)
       (-> (date/last-day-of-the-month year month)
           date/day
           (- 7)))))

(defn is-in-weektype? [a-date weektype]
  (case weektype
    "teenth" (is-in-teenthweek? a-date)
    "last"   (is-in-last-week? a-date)
    (is-in-week-num? a-date (week-number weektype))))

(defn is-weekday-of-weektype? [a-date weekday weektype]
  "Is the date both on the required weekday (eg. 'monday'), and weektype (eg. 'first')?
  Ex: is it the first monday of the week?"
  ; (break)
  (and (is-weekday? a-date weekday)
       (is-in-weektype? a-date weektype)))

(defn find-date-for-weekday-in-weektype [weekday weektype month year]
  ; Beware: internally, all the dates take the year first, and the the month. Externally, the API takes them in opposite order
  "This function finds the date which correspond to the required weekday (eg. 'monday') and weektyps (eg. 'first'), on the given year and month"
  (->> (date/first-day-of-the-month year month)
       (iterate #(date/plus % (date/days 1))) ; all subsequent dates
       (find-first #(is-weekday-of-weektype? % weekday weektype))))

(defn format-date-YYYY-M-D [a-date]
  "Example: [2013 5 17]"
  ; (break)
  ((juxt date/year date/month date/day) a-date))


;;;; Create the functions

(defn final-fn-name [weekday weektype]
  "\"first-monday\", \"second-monday\", \"monteenth\", etc"
  (if (= weektype "teenth")
    (str (dayname->fn-prefix weekday) "teenth")
    (str weektype "-" weekday)))

(defmacro defn-all-API-fns []
  `(do ; Execute all the defn statements
    ~@(for [weektype weektypes
            weekday  weekdays]
      `(defn ~(symbol (final-fn-name weekday weektype)) [month# year#]
        (format-date-YYYY-M-D
          (find-date-for-weekday-in-weektype ~weekday ~weektype month# year#))))))

(defn-all-API-fns)

; (defn monteenth [month year] "yes!")
