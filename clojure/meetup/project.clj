(defproject meetup "1.0.0"
  :dependencies [
    [org.clojure/clojure "1.6.0"]
    [clj-time "0.7.0"]
    [mascip.debug "0.1.0"]
    [medley "0.5.0"]
  ]
  :profiles {
    ; :dev {:source-paths ["dev"]}
    :dependencies [[org.clojure.tools/namespace "0.2.4"]]
    }
  )
