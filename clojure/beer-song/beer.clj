(ns beer
  (require
    [clojure.string :as s]
    [clojure.data :as d]))

(declare c cc diff) ; Debug functions declared below

(defn say-bottles [n]
  (case n
    0 "no more bottles of beer"
    1 "1 bottle of beer"
      (str n " bottles of beer")))

(defn say-action [n]
  (case n
    0     "Go to the store and buy some more"
    1     "Take it down and pass it around"
          "Take one down and pass it around"))

(defn verse [n]
  (let [n2 (if (= n 0) 99 (dec n))]  ; After singing for 0 bottle, we sing for 99  
    (str (s/capitalize (say-bottles n)) " on the wall, "
         (say-bottles n) ".\n" 
         (say-action n) ", "
         (say-bottles n2) " on the wall.\n")))

(defn sing
  ([end-n] (sing end-n 0))
  ([end-n start-n]
    (->> (range end-n (dec start-n) -1) (c "Verse numbers")
         (map verse)                    (c "Verses")
         (s/join "\n")                  (c "Add newlines"))))
; NOTE: by "switching on" the (c) function, you get a pretty debugging output here.


;;;; DEBUG FUNCTIONS
; I created these to help me debug. Please let me know if there are better alternatives around, or in the REPL

(defn c [s data]
  "(c) and (cc) are great for debugging in a ->> thread macro.
  (c) takes the data it receives, and just passes it down.
  (cc) does the same, but prints the given String and the Data.
  You can easily turn (c) into (cc) in your code, in order to get some debug info.
  If you want to \"switch on\" all (c) functions so they all print data, you can just uncomment that one line in the (c) function source code. In the near future this will be implemented with a hook, and so that you can switch it on in a given scope without needing to touch its source code.
  PS: I (mascip) would be very happy for someone to show me a better way to do this, perhaps throught the REPL?
  PPS: The name (c) stands for \"comment\", because it looks like a comment in the code. But a useful one for debugging."
  ; (println s) (prn data) ; ***UNCOMMENT*** this line to get debugging (will be replaced by an eternal hook, but I can't use a hook in exercism.io)
  data)

(defn cc [s data]
  "(c) and (cc) are great for debugging in a ->> thread macro.
  (c) takes the data it receives, and just passes it down.
  (cc) does the same, but prints the given String and the Data.
  You can easily turn (c) into (cc) in your code, in order to get some debug info.
  If you want to \"switch on\" all (c) functions so they all print data, you can just uncomment that one line in the (c) function source code. In the near future this will be implemented with a hook, and so that you can switch it on in a given scope without needing to touch its source code.
  PS: I (mascip) would be very happy for someone to show me a better way to do this, perhaps throught the REPL?
  PPS: The name (c) stands for \"comment\", because it looks like a comment in the code. But a useful one for debugging."
  (println s) (prn data)
  data)

(defn diff [s1 s2]
  "To find where is the difference between two strings:
  (diff \"fabler\" \"cable\") => \"f    r"\"
  (->> [s1 s2]
       (map (partial into []))  ; str->chars
       (apply d/diff)           ; diff
       (first)                  ; only keep diff on first string
       (map #(if (= % nil) " " %))(s/join))) ; chars->str. nil elements are replaced by a space
