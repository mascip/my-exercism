(ns word-count
  (:require
    [clojure.string :as s :only [replace]]
  )
)

(defn clean [s]
  "Separate the string into a sequence of words, lower-cased"
  (-> s s/lower-case (s/split #"\W+")))

(defn word-count [s]
  (-> s clean frequencies))
