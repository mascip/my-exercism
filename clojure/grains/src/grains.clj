(ns grains)

(defn square [n] (->> (take (dec n) (repeat 2))
                      (reduce *')))
  ; the *' function autopromotes to bigint upon overflow

(defn total [] (->> (range 1 65)
                    (map square)
                    (apply +)))
