(defproject leap "0.5.0-SNAPSHOT"
  :description "A project for doing things."
  :license "Eclipse Public License 1.0"
  :url ""
  :main leap
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :plugins [
    ; $lein test-refresh, wil re-run all the tests when any file is modified
    [com.jakemccrary/lein-test-refresh "0.5.0"]
    ; in the REPL, the namespaces will be re-loaded when any file is modified
    [lein-autoreload "0.1.0"]
    ]
)
