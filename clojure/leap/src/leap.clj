(ns leap)

(defn multiple-of? [divisor n]
  (-> n (rem divisor) (zero?)))

(defn leap-year? [y]
  ( as-> y Y
         (or (multiple-of? 400 Y)
             (and (multiple-of? 4 Y)
                  (not (multiple-of? 100 Y))))))
