(ns dna (:require [clojure.set :as set]))

(def dna-nucleotides #{\A \C \G \T})
(def rna-nucleotides #{\A \C \G \U})
(def all-nucleotides (set/union dna-nucleotides rna-nucleotides))

; A hashmap of the DNA nucleotides, with 0 as values
; eg: {\A 0, \C 0, ....}
(def empty-count-nucleo (zipmap dna-nucleotides (repeat 0)))

(defn nucleotide-counts [s]
  (->> s (frequencies)
         (merge empty-count-nucleo))) ; Default value: 0

(defn count [c s]
  (when-not (all-nucleotides c)
    (throw (Exception. "invalid nucleotide")))
  (get (nucleotide-counts s) c 0))
    ; Default value is 0, for nucleotides who are part of RNA,
    ; but not part of DNA
