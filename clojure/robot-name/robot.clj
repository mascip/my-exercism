(ns robot
  (:require [clojure.string :as s]))

(def range-AZ
  (->> (range 26)
       (map (partial + (int \A)))
       (map char)))

(def possible-names
  (for [a1 range-AZ, a2 range-AZ,
        n1 (range 10), n2 (range 10), n3 (range 10)]
    (str a1 a2 n1 n2 n3)))

(def index (atom 0))

(defn next-name []
  (swap! index inc) 
  (nth possible-names @index))

(defn robot []
  (atom {:name (next-name)}))

(defn robot-name [r] (:name @r))

(defn reset-name [r]
  (swap! r assoc :name (next-name))
  )
