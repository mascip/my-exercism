(ns phone (:require [clojure.string :as s]))

(defn valid-11-digit-number? [digits]
  (and  (= 11 (count digits)) (= \1 (first digits))))

(defn number [s]
  (let [digits (s/replace s #"\D" "")]
    (cond
       (= 10 (count digits))           digits
       (valid-11-digit-number? digits) (subs digits 1)
       :else                           "0000000000")))

(defn area-code [n] (subs n 0 3))

(defn pretty-print [s]
  (as-> (number s) n
        (str "(" (area-code n) ") " (subs n 3 6) "-" (subs n 6 10))))

