(ns anagram
  (:require
    [clojure.string :as s]
  )
)

;;;; SOLUTION 1: More readable
(defn same-unordered-chars? [w1 w2]
  (= (frequencies w1) (frequencies w2)))

(defn anagram? [w1 w2]
  "Two anagram words must contain exactly the same characters (same number of occurences), but they cannot be the same word"
  (let [lc1 (s/lower-case w1)
        lc2 (s/lower-case w2)]
    (and (not= lc1 lc2)
         (same-unordered-chars? lc1 lc2))))

(defn anagrams-for [w words]
  (filter (partial anagram? w) words))


;;;; SOLUTION 2: More modular but bad because less readable - can it be improved?

(defn same-unordered-chars2?
  ; Two words
  ([w1 w2] (->> [w1 w2] (map frequencies) (apply =) ))
  ; One vector of two words
  ([[w1 w2]] () (same-unordered-chars? w1 w2)))

(defn same-word2?
  ; Two words
  ([w1 w2] (= w1 w2))
  ; One vector of two words
  ([[w1 w2]] (same-word2? w1 w2)))

(defn anagram2? [w1 w2]
  "Two anagram words must contain exactly the same characters (same number of occurences), but they cannot be the same word"
  (->> [w1 w2]
       (map s/lower-case)
       ((every-pred
          (apply complement same-word2?)   ;Not the same word...
          same-unordered-chars2?)))) ;...but the same chars

(defn anagrams-for2 [w words]
  (filter (partial anagram2? w) words))

; In this solution I tried to not use function names and variable
; names more than once. Also, I had trouble passing a vector of
; data to the two functions same-word? and same-unordered-chars?,
; so I had to let these two functions take a vector as argument, 
; which I think is not an elegant solution.
; Is there a better way, to pass this vector as two separate argument, in a thread?

; For instance this works:
; (->> [1 2] (apply =))
; And this works:
; (-> 5 ((every-pred odd? odd?)))
; But this doesn't:
; (->> [1 2] ((every-pred (apply =) (apply =))))
; and I don't understand why

; But well, the solution as it is works... I just hope to find a way to improve it.

