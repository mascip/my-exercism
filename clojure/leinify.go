package main
<<<<<<< HEAD

=======
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
import "errors"
import "fmt"
import "io"
import "io/ioutil"
import "log"
import "os"
import "path/filepath"
import "strings"
<<<<<<< HEAD

func main() {
  testFile, err := detectExercism()

  if (err != nil) {
    log.Fatal(err)
  }

  fmt.Println("Found " + testFile + ", let the leinifying commence!")

  exerciseName := strings.Replace(testFile, "_test.clj", "", -1)
  fmt.Println("Exercise name: " + exerciseName)

=======
 
func main() {
  testFile, err := detectExercism()
 
  if (err != nil) {
    log.Fatal(err)
  }
 
  fmt.Println("Found " + testFile + ", let the leinifying commence!")
 
  exerciseName := strings.Replace(testFile, "_test.clj", "", -1)
  fmt.Println("Exercise name: " + exerciseName)
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  transformTestFile(testFile, exerciseName)
  createProjectFile(exerciseName)
  createSrcFile(exerciseName)
}
<<<<<<< HEAD

func detectExercism() (f string, err error) {
  files, _ := filepath.Glob("*_test.clj")

  if len(files) == 0 {
    return "", errors.New("Could not find an exercism clojure test (*_test.clj) in current directory.")
  }

  return files[0], nil
}

=======
 
func detectExercism() (f string, err error) {
  files, _ := filepath.Glob("*_test.clj")
 
  if len(files) == 0 {
    return "", errors.New("Could not find an exercism clojure test (*_test.clj) in current directory.")
  }
 
  return files[0], nil
}
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
func transformTestFile(fileName string, exerciseName string) {
  bytes, err := ioutil.ReadFile(fileName)
  if (err != nil) {
    log.Fatal(err)
  }
  content := string(bytes[:])
<<<<<<< HEAD

  // extract ns form boundaries
  _, nsBoundEnd := formBounds(content, 0)

  // extract load file form boundaries
  _, lfBoundEnd := formBounds(content, nsBoundEnd+1)

  // remove ns and load-file forms
  content = content[lfBoundEnd+1:]

=======
 
  // extract ns form boundaries
  _, nsBoundEnd := formBounds(content, 0)
 
  // extract load file form boundaries
  _, lfBoundEnd := formBounds(content, nsBoundEnd+1)
 
  // remove ns and load-file forms
  content = content[lfBoundEnd+1:]
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  // insert new ns form
  nsForm := "(ns " + exerciseName + "-test\n" +
            "  (:require [clojure.test :refer :all]\n" +
            "            [" + exerciseName + " :refer :all]))\n\n"
<<<<<<< HEAD

  content = nsForm + strings.Replace(content, "(run-tests)", "", -1)

  if os.MkdirAll("test", 0777) != nil {
    log.Fatal("could not create 'test' directory!")
  }

=======
 
  content = nsForm + strings.Replace(content, "(run-tests)", "", -1)
 
  if os.MkdirAll("test", 0777) != nil {
    log.Fatal("could not create 'test' directory!")
  }
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  fileOut, err := os.Create(strings.Join([]string{"test", fileName}, "/"))
  if (err != nil) {
    log.Fatal("could not create 'test/" + fileName + "'!")
  }
  defer fileOut.Close()
<<<<<<< HEAD

  fmt.Println("writing 'test/" + fileName + "'...")
  io.WriteString(fileOut, content)
}

=======
 
  fmt.Println("writing 'test/" + fileName + "'...")
  io.WriteString(fileOut, content)
}
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
// find the start and end character index of the next lisp form in string
// at or after index, handling nested forms
func formBounds(s string, index int) (startIndex int, endIndex int) {
  stack := 0
  startIndex = -1
  endIndex = -1
<<<<<<< HEAD

=======
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  for i,c := range s {
    if i >= index {
      switch c {
      case '(':
        if (startIndex == -1) {
          startIndex = i
        }
        stack++
      case ')':
        stack--
        if (stack == 0) {
          endIndex = i
          break
        }
      }
    }
<<<<<<< HEAD

=======
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
    if (endIndex > 0) {
      break
    }
  }
<<<<<<< HEAD

  return startIndex, endIndex+1
}

=======
 
  return startIndex, endIndex+1
}
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
func createProjectFile(exerciseName string) {
  fileOut, err := os.Create("project.clj")
  if (err != nil) {
    log.Fatal("could not create 'project.clj'!")
  }
  defer fileOut.Close()
<<<<<<< HEAD

=======
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  clojureVersion := os.Getenv("CLOJURE_VERSION")
  if (clojureVersion == "") {
    message := "*** It looks like you haven't specified your clojure version.\n" +
               "    I'll default it to 1.6.0; if you want something different,\n" +
               "    set the CLOJURE_VERSION environment variable"
    fmt.Println(message)
    clojureVersion = "1.6.0"
  }
<<<<<<< HEAD

  content := "(defproject " + exerciseName + " \"1.0.0\"\n" + 
             "  :dependencies [[org.clojure/clojure \"" + clojureVersion + "\"]])\n"

  fmt.Println("writing 'project.clj'...")
  io.WriteString(fileOut, content)
}

=======
 
  content := "(defproject " + exerciseName + " \"1.0.0\"\n" + 
             "  :dependencies [[org.clojure/clojure \"" + clojureVersion + "\"]])\n"
 
  fmt.Println("writing 'project.clj'...")
  io.WriteString(fileOut, content)
}
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
func createSrcFile(exerciseName string) {
  if os.MkdirAll("src", 0777) != nil {
    log.Fatal("could not create 'src' directory!")
  }
<<<<<<< HEAD

  fileName := exerciseName + ".clj"

=======
 
  fileName := exerciseName + ".clj"
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  fileOut, err := os.Create(strings.Join([]string{"src", fileName}, "/"))
  if (err != nil) {
    log.Fatal("could not create 'src/" + fileName + "'!")
  }
  defer fileOut.Close()
<<<<<<< HEAD

=======
 
>>>>>>> 5ee35155538a511bb316c779d507c211f7d8023c
  fmt.Println("writing 'src/" + fileName + "'...")
  io.WriteString(fileOut, "(ns " + exerciseName + ")")
}
