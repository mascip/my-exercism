(ns space_age)

(defn on-earth [seconds]
  "Converts seconds into years"
  (float (/ seconds 3600 24 365.25))) ; There are 365.25 days in a year

(def orbital-period-of ; In Earth years
  {:mercury  0.2408467, :venus 0.61519726, :mars 1.8808158, :jupiter 11.862615 , :saturn 29.447498 , :uranus 84.016846 , :neptune 164.79132 })

(defn on-planet [planet seconds]
  (-> seconds on-earth (/ (orbital-period-of planet))))

(defmacro defn-all-API-fns []
  `(do ; Execute all the defn statements
    ~@(for [planet (map name (keys orbital-period-of))]

      `(defn ~(symbol (str "on-" planet)) [seconds#]
        (on-planet ~(keyword planet) seconds#)))))

(defn-all-API-fns)
