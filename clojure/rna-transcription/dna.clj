(ns dna)

(defn transcribe [c]
  (case c
    \G \C
    \C \G
    \A \U
    \T \A
    (assert false))) ; Error

(defn to-rna [s]
  (->> s (map transcribe) (apply str)))
