(defproject plugthing "0.1.0-SNAPSHOT"
  :description "Plug Thing game from the Brave and True Clojure tutorial"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :plugins []
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}} 
)
