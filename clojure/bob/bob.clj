(ns bob
  (:require 
    [clojure.string :refer [lower-case blank?] :as s]
  )
)

(defn lower-case? [c]
  (Character/isLowerCase c))

(defn capital-case? [c]
  (Character/isUpperCase c))

(defn yell? [s]
  (and (not-any? lower-case? s) (some capital-case? s)))

(defn question? [s]
  (= \? (last s)))

(defn response-for [s]
  (cond
    (s/blank? s)  " Fine. Be that way!"
    (yell? s)     " Woah, chill out!"
    (question? s) " Sure."
    :else         " Whatever."))
