(ns dna)
(declare c cc)


(defn hamming-distance [a b]
  (->> [a b]            (c "Words")
       (apply map =)    (c "Map equal")
       (filter false?)  (c "Filter")
       (count)          (c "Count")))

(defn c  [s data]
    "(c) and (cc) are great for debugging in a ->> thread macro.
      (c) takes the data it receives, and just passes it down.
      (cc) does the same, but prints the given String and the Data.
      You can easily turn (c) into (cc) in your code, in order to get some debug info.
      If you want to \"switch on\" all (c) functions so they all print data, you can just uncomment that one line in the (c) function source code. In the near future this will be implemented with a hook, and so that you can switch it on in a given scope without needing to touch its source code.
      PS: I (mascip) would be very happy for someone to show me a better way to do this, perhaps throught the REPL?
      PPS: The name (c) stands for \"comment\", because it looks like a comment in the code. But a useful one for debugging."
      ; (println s)  (prn data) ; ***UNCOMMENT*** this line to get debugging (will be replaced by an eternal hook, but I can't use a hook in exercism.io)
        data)

(defn cc  [s data]
    "(c) and (cc) are great for debugging in a ->> thread macro.
      (c) takes the data it receives, and just passes it down.
      (cc) does the same, but prints the given String and the Data.
      You can easily turn (c) into (cc) in your code, in order to get some debug info.
      If you want to \"switch on\" all (c) functions so they all print data, you can just uncomment that one line in the (c) function source code. In the near future this will be implemented with a hook, and so that you can switch it on in a given scope without needing to touch its source code.
      PS: I (mascip) would be very happy for someone to show me a better way to do this, perhaps throught the REPL?
      PPS: The name (c) stands for \"comment\", because it looks like a comment in the code. But a useful one for debugging."
      (println s)  (prn data)
      data)
