(ns etl
  (:require [clojure.string :as s]
            [robert.hooke :refer :all]
            [mascip.debug :refer :all]
            [clojure.tools.trace :refer :all]
            ))

(deftrace transform [m]
  (with-scope (add-hook #'c #'c-print)
  (->> m (map (fn [[score letters]]
           (zipmap (map s/lower-case letters) (repeat score)))) (c "inverse and interleave words and scores")
         (apply merge) (c "merge")))
)

(defn print-it [s] (->> s (s/reverse) (c "PRINT-IT C FUNCTION") (println)))

(defn combine-them []
  (transform {1 ["a"]})
  (print-it "inverse me")
  (transform {2 ["b"]})
)
